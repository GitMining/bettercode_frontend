import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueResource from 'vue-resource'
Vue.use(VueResource)

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element)

import router from './router'

import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: '',
    platform: '',
    projectId: '',
    remotePlatform: '',
    projectOwner: '',
    projectName: '',
    // baseUrl: "http://localhost:8080",
    baseUrl: "http://47.101.137.127:8080",
    issues: '',
    devs: '',
    last: '',
    info:0,
    warning:0,
    eooro:0
  },
  mutations: {
    saveUser(state, username) {
      state.user = username
    },
    savePlatform(state, platform) {
      state.platform = platform
    },
    saveProjectId(state, projectId) {
      state.projectId = projectId
    },
    saveRemotePlatform(state, remotePlatform) {
      state.remotePlatform = remotePlatform
    },
    saveProjectOwner(state, projectOwner) {
      state.projectOwner = projectOwner
    },
    saveProjectName(state, projectName) {
      state.projectName = projectName
    },
    saveIssues(state, issues) {
      state.issues = issues
    },
    saveDevs(state, devs) {
      state.devs = devs
    },
    saveLast(state, last) {
      state.last = last
    },
    saveInfo(state, info){
      state.info = info
    },
    saveWarning(state, warning){
      state.warning = warning
    },
    saveError(state, error){
      state.error = error
    }
  }
})

new Vue({
  el: '#app',
  store,
  router: router,
  render: h => h(App)
})
